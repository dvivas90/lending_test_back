from tornado.gen import coroutine

class LoanCalculate:

     @coroutine
     def calculateState(amount):
        if (amount > 50000): return 'DECLINED'
        if (amount == 50000): return 'UNDECIDED'
        if (amount < 50000): return 'APPROVED'




