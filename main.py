import tornado.ioloop
import tornado.web
import tornado.escape

from handlers.LoanHandler import LoanHandler
from handlers.OwnerHandler import OwnerHandler
from handlers.BusinessHandler import BusinessHandler


def make_app():
    return tornado.web.Application([
        (r"/request", LoanHandler),
        (r"/owner", OwnerHandler),
        (r"/business", BusinessHandler)
    ])

if __name__ == '__main__':
    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()