
from typing import Optional, Awaitable
import tornado.web
import tornado.escape
from utils.LoanCalculate import LoanCalculate
from tornado.gen import coroutine


class LoanHandler(tornado.web.RequestHandler):

    def data_received(self, chunk: bytes) -> Optional[Awaitable[None]]:
        pass

    def get(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.write({'message': 'Test get'})

    @coroutine
    def post(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        data = tornado.escape.json_decode(self.request.body)

        # Validates if amount is present
        if data.get('amount') is None:
            self.set_status(400)
            self.finish("Amount required")
        else:
            try:
                amount = int(data.get('amount'))
                res = yield LoanCalculate.calculateState(amount)
                self.write(res)
            except ValueError:
                self.set_status(400)
                self.finish("Invalid number")