from typing import Optional, Awaitable

import tornado.web
import tornado.escape
from models.Business import Business
import json


class BusinessHandler(tornado.web.RequestHandler):

    def data_received(self, chunk: bytes) -> Optional[Awaitable[None]]:
        pass

    def post(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        data = tornado.escape.json_decode(self.request.body)
        j= json.loads(self.request.body);
        business = Business(**j)
        self.write(business.taxId)

